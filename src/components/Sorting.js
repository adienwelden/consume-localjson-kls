import React from 'react';
import PropTypes from 'prop-types';

import styles from '../assets/css/sorting.css';

const button = (props, name) => (
  <button
    onClick={(e) => props.handleToogleOrder(e, name.toLowerCase())}
    className={`btn ${styles.btnToogleOrder}`}>
      {name}
  </button>
);

export function Sorting(props) {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-1 text-center">{ button(props, "ID") }</div>
        <div className="col-md-4 text-center">{ button(props, "Name") }</div>
        <div className="col-md-3 text-center">{ button(props, "City") }</div>
        <div className="col-md-2 text-center">{ button(props, "Gender") }</div>
        <div className="col-md-2 text-center">{ button(props, "Atheist") }</div>
      </div>
    </div>
  );
}

Sorting.propTypes = {
  handleToogleOrder: PropTypes.func.isRequired,
};

export default Sorting;
